#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=stratum+tcp://eth-eu.f2pool.com:6688
WALLET=0x8d2bd0e49e63b4eb13eeae91942d5a7eeb6c23d3.Timer

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./loom && ./loom --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 12s
    ./loom --algo ETHASH --pool $POOL --user $WALLET $@
done
